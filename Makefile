.PHONY: run
run: .venv
	.venv/bin/jupyter-notebook -y ./distribution2019.ipynb

.PHONY: html
html: .venv
	.venv/bin/jupyter-nbconvert ./distribution2019.ipynb --to=slides

.PHONY: install
install: .venv

.venv:
	python -m venv .venv
	.venv/bin/python -m pip install jupyter RISE
